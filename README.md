# extra-tabler-icons

Adapted icons to complement [Tabler Icons](https://tabler.io/icons).

- [Source code](https://gitlab.com/joaommpalmeiro/extra-tabler-icons)
- [npm package](https://www.npmjs.com/package/extra-tabler-icons)
- [Licenses](https://licenses.dev/npm/extra-tabler-icons)
- [Package Phobia](https://packagephobia.com/result?p=extra-tabler-icons)
- [bundlejs](https://bundlejs.com/?bundle&q=extra-tabler-icons)
- [npm trends](https://npmtrends.com/extra-tabler-icons)
- [Snyk Advisor](https://snyk.io/advisor/npm-package/extra-tabler-icons)
- [Visualization of npm dependencies](https://npm.anvaka.com/#/view/2d/extra-tabler-icons)

## Available icons

> Based on Tabler Icons [v3.14.0](https://github.com/tabler/tabler-icons/releases/tag/v3.14.0)

### Brands

- Feedzai: `feedzai.svg`
  - https://feedzai.com/newsroom/
- JupyterLab: `jupyterlab.svg`
  - https://github.com/tabler/tabler-icons/blob/v3.14.0/icons/outline/dots.svg
  - https://github.com/tabler/tabler-icons/blob/v3.14.0/icons/outline/brand-vue.svg
  - https://github.com/catppuccin/vscode-icons/blob/v1.15.0/icons/frappe/jupyter.svg

### Two-letter icons

- Codeberg: `cb.svg`
  - https://github.com/tabler/tabler-icons/blob/v3.14.0/icons/outline/letter-c-small.svg
  - https://github.com/tabler/tabler-icons/blob/v3.14.0/icons/outline/letter-b-small.svg
- GitHub: `gh.svg`
  - https://github.com/tabler/tabler-icons/blob/v3.14.0/icons/outline/letter-g-small.svg
  - https://github.com/tabler/tabler-icons/blob/v3.14.0/icons/outline/letter-h-small.svg
- GitLab: `gl.svg`
  - https://github.com/tabler/tabler-icons/blob/v3.14.0/icons/outline/letter-g-small.svg
  - https://github.com/tabler/tabler-icons/blob/v3.14.0/icons/outline/letter-l-small.svg
- Hugging Face: `hf.svg`
  - https://github.com/tabler/tabler-icons/blob/v3.14.0/icons/outline/letter-h-small.svg
  - https://github.com/tabler/tabler-icons/blob/v3.14.0/icons/outline/letter-f-small.svg

## Development

Install [fnm](https://github.com/Schniz/fnm) (if necessary).

```bash
fnm install && fnm use && node --version && npm --version
```

```bash
npm install
```

```bash
npm run dev
```

```bash
npm run lint
```

```bash
npm run format
```

## Deployment

```bash
npm pack --dry-run
```

```bash
npm version patch
```

```bash
npm version minor
```

```bash
npm version major
```

```bash
echo "v$(npm pkg get version | tr -d \")" | pbcopy
```

- Commit and push changes.
- Create a tag on [GitHub Desktop](https://github.blog/2020-05-12-create-and-push-tags-in-the-latest-github-desktop-2-5-release/).
- Check [GitLab](https://gitlab.com/joaommpalmeiro/extra-tabler-icons/-/tags).

```bash
npm login
```

```bash
npm publish
```

- Check [npm](https://www.npmjs.com/package/extra-tabler-icons).
