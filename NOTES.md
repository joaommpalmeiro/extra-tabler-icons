# Notes

- https://github.com/joaopalmeiro/social-media-icons
- https://github.com/joaopalmeiro/template-ts-package
- https://gitlab.com/joaommpalmeiro/resetss
- https://fonts.google.com/specimen/Lilita+One
- https://www.widelands.org/wiki/GitPrimer/: "(...) (Codeberg and GitHub), please also prefix it with "GH" or "CB" (...)"
- https://developer.mozilla.org/en-US/docs/Web/SVG/Tutorial/Paths
- https://catppuccin.com/palette
- https://github.com/tabler/tabler-icons
- https://docs.astro.build/en/reference/publish-to-npm/
- https://github.com/prettier/prettier/issues/5322:
  - https://github.com/prettier/prettier/issues/5322#issuecomment-1450182797: `prettier --write --parser html "**/*.svg"`
  - https://github.com/prettier/plugin-xml
  - https://prettier.io/docs/en/configuration.html#setting-the-parserdocsenoptionshtmlparser-option
- https://github.com/tabler/tabler-icons/releases
- **Two-letter icons**:
  - Steps:
    1. Subtract 3.5 to the `M` command from the first letter `<path>` node(s)
    2. Add 3.5 to the `M` command from the second letter `<path>` node(s)
  - References:
    - Based on the icon/letter spacing of the [`svg`](https://github.com/tabler/tabler-icons/blob/v3.14.0/icons/outline/svg.svg) and [`math-tg`](https://github.com/tabler/tabler-icons/blob/v3.14.0/icons/outline/math-tg.svg) icons
    - https://github.com/tabler/tabler-icons/blob/v3.14.0/icons/outline/number-30-small.svg
    - https://github.com/tabler/tabler-icons/blob/v3.14.0/icons/outline/number-100-small.svg
- https://tabler.io/icons/icon-design-guide
- https://github.com/tabler/tabler-icons/blob/v3.2.0/.build/helpers.mjs
- https://github.com/tabler/tabler-icons/blob/v3.2.0/.build/optimize.mjs
- https://developer.mozilla.org/en-US/docs/Web/CSS/color_value#currentcolor_keyword: `currentcolor`
- https://tabler.io/changelog
- https://github.com/tabler/tabler-icons/blob/v3.2.0/icons/outline/abacus.svg?short_path=6560b17
- `stroke="red" opacity="0.2"`
- Lucide guidelines:
  - https://lucide.dev/guide/design/icon-design-guide#icon-design-principles
  - "7. Shapes (such as rectangles) must have a border radius of"
    - "A. 2 pixels if they are at least 8 pixels in size"
    - "B. 1 pixel if they are smaller than 8 pixels in size"
  - "8. Distinct elements must have 2 pixels of spacing between each other"
  - "9. Icons should have a similar optical volume to `circle` and `square`."
  - "10. Icons should be visually centered by their center of gravity."
    - "Tip: place your icon both above/below and next to the square or circle icon and check if it feels off center. Symmetrical icons should always be aligned to the center."
  - "11. Icons should have similar visual density and level of detail."
  - "Never use `<use>`. While it may sometimes seem like a good way to optimize file size, there's no way to ensure that the referenced element IDs will be unique once the SVGs are embedded in HTML documents."

## Commands

```bash
npm install -D \
npm-package-json-lint \
npm-package-json-lint-config-package \
npm-run-all2 \
prettier \
publint \
sort-package-json \
svglint \
svgo \
tsx
```

```bash
npm install -D "@types/node@$(cat .nvmrc | cut -d . -f 1-2)"
```

## Snippets

```js
export const iconTemplate = (type) =>
  type === "outline"
    ? `<svg
  xmlns="http://www.w3.org/2000/svg"
  width="24"
  height="24"
  viewBox="0 0 24 24"
  fill="none"
  stroke="currentColor"
  stroke-width="2"
  stroke-linecap="round"
  stroke-linejoin="round"
>`
    : `<svg
  xmlns="http://www.w3.org/2000/svg"
  width="24"
  height="24"
  viewBox="0 0 24 24"
  fill="currentColor"
>`;
```
