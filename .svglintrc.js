export default {
  rules: {
    elm: {
      svg: 1,
      "svg > path": [1, 5],
      "*": false,
    },
    attr: [
      {
        xmlns: "http://www.w3.org/2000/svg",
        width: "24",
        height: "24",
        viewBox: "0 0 24 24",
        fill: "none",
        stroke: "currentcolor",
        "stroke-width": "2",
        "stroke-linecap": "round",
        "stroke-linejoin": "round",
        "rule::selector": "svg",
        "rule::whitelist": true,
        "rule::order": [
          "xmlns",
          "width",
          "height",
          "viewBox",
          "fill",
          "stroke",
          "stroke-width",
          "stroke-linecap",
          "stroke-linejoin",
        ],
      },
      {
        d: true,
        "rule::selector": "svg > path",
        "rule::whitelist": true,
      },
    ],
    valid: true,
  },
};
