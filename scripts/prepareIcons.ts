import { readdir, readFile, mkdir, writeFile } from "node:fs/promises";
import path from "node:path";
import type { Config } from "svgo";
import { optimize } from "svgo";

const INPUT_DIR = path.join(import.meta.dirname, "..", "icons", "figma");
const OUTPUT_DIR = path.join(import.meta.dirname, "..", "icons");

const TABLER_SVG_ATTRIBUTES: Record<string, string>[] = [
  { fill: "none" },
  { stroke: "currentcolor" },
  { "stroke-width": "2" },
  { "stroke-linecap": "round" },
  { "stroke-linejoin": "round" },
];

const SVGO_OPTIONS: Config = {
  multipass: true,
  plugins: [
    "preset-default",
    "cleanupListOfValues",
    {
      name: "removeAttrs",
      params: {
        attrs: TABLER_SVG_ATTRIBUTES.flatMap((attribute) =>
          Object.keys(attribute),
        ),
      },
    },
    {
      name: "addAttributesToSVGElement",
      params: {
        attributes: TABLER_SVG_ATTRIBUTES,
      },
    },
  ],
};

async function readDirFiles(dirPath: string): Promise<string[]> {
  const dirContent = await readdir(dirPath, {
    recursive: true,
    withFileTypes: true,
  });

  const dirFiles = dirContent.flatMap((content) => {
    return content.isFile()
      ? [path.join(content.parentPath, content.name)]
      : [];
  });

  return dirFiles;
}

async function ensureDir(dirPath: string): Promise<void> {
  await mkdir(dirPath, { recursive: true });
}

async function writeSvg(filePath: string, content: string): Promise<void> {
  const svgPath = path.extname(filePath)
    ? filePath
    : path.format({
        name: filePath,
        ext: "svg",
      });

  await ensureDir(path.dirname(svgPath));
  await writeFile(svgPath, content);
}

async function processIcon(iconPath: string): Promise<void> {
  const content = await readFile(iconPath, { encoding: "utf8" });
  const { data: optimizedContent } = optimize(content, SVGO_OPTIONS);

  const outputPath = path.join(OUTPUT_DIR, path.basename(iconPath));

  await writeSvg(outputPath, optimizedContent);
  console.info(`${path.basename(iconPath)} ✓`);
}

async function main(): Promise<void> {
  const icons = await readDirFiles(INPUT_DIR);

  await Promise.all(icons.map(processIcon));
}

main();
